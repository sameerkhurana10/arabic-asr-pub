# Automatic Speech Recognition

Joint work by MIT and QCRI

# MGB

## Setup

* Location : `/data/sls/qcri/asr/sameer_v1/asr/s5_ar`

## Dataset
MGB2

* 300k training utterances, 5k dev utterances. Backgoroung LM data, 950k lexicon size

MGB3

* Adaptation/Training: 1972 utterances
* Dev.Ali : 2k utterances

### LM

LM is trained on all background and training text (300k)

**MGB2 LMs**
PPL calculated on dev text utterances (5002 sentences, 60169 Words)

| Model  | PPL | PPL1 | OOVS |
| ------------- | ------------- | ------------- | ------------- |
| 3-gram  | 1524.04 | 2833.659 | 1059 |
| 4-gram  | 1145.3 | 2078.599 | 1059 |
| 5-gram  |  1145.3 | 2078.599 | 1059 |
| RNNLM   | -  |- |- |



**MIX MGB3 and MGB2 Language Models**

| Model  | mixing-prop | prune | PPL-dev.ali.mgb3 | PPL1-dev.ali.mgb3 | PPL-dev.mgb2 | PPL1-dev.mgb2 | OOVS-mg3 | OOVS-mg2 |
| ------------- | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- |
| 4-gram-mgb2 (A)  | | 1e-7| 5272.4 | 8845.9 | 1489.1 | 2763.2 |  1628 | 1059 |
| 4-gram-mgb3 (B)  | |1e-7 | 5215.4 | 8744.6 | 32224.3 | 77566.7 | 1628 | 1059 |
| 4-gram-mix  | 0.9*B | - | 1262.3 | 1942.6 | 1659.2 | 3107.3 | 1628 | 1059 |
| 4-gram-mix  | 0.7*B | - | 1262.3 | 1942.6 | 1659.2 | 3107.3 | 1628 | 1059 |
| 4-gram-mix  | 0.5*B | - | **1161.3** | 1778.3 | 1293.8 | 2372.4 | 1628 | 1059 |
| 4-gram-mix  | 0.3*B | - | 1178.1 | 1805.5 | 1140.8 | 2069.7 | 1628 | 1059 |
| 4-gram-mix  | 0.9*B | 1e-7 | 3367.2 | 5498.4 | 7164.5 | 15185.2 | 1628 | 1059 |
| 4-gram-mix   | 0.7*B | 1e-7 | 2573.9 | 4135.4 | 3077.6 | 6072.8 | 1628 | 1059 |
| 4-gram-mix  | 0.5*B | 1e-7 | **2423.6** | 3879.9 | 2083.9 | 3978.6 | 1628 | 1059 |
| 4-gram-mix  | 0.3*B | 1e-7 | 2546.7 | 4089.1 | 1658.7 | 3106.4 | 1628 | 1059 |

### Decoding

| Model  | Data | LM | mgb2-wer-dev | mgb3-wer-dev.Ali | 
| ------------- | ------------- | ------------- | ------------- | ------------- |
| triphone-sat  | mgb2 | pruned-3gram | 40.2 | - |
| triphone-sat  | mgb2 | pruned-4gram | 39.5 | - |
| chain-tdnn-attention  |- | pruned-4gram | - | - |
| chain-tdnn-lstm  | - | pruned-4gram | titan-4 | - |
| chain-tdnn-blstm  | mgb2-aug | pruned-4gram-mgb2 | 19.5 (titan-0) | 58.7 (**WIP**) |
| chain-tdnn-blstm  | mgb2-aug-reverb | pruned-4gram | titan-5 | - |
| chain-tdnn-lstm  | mgb2-aug-reverb | pruned-4gram | titan-2 | - |
| chain-tdnn-blstm  | mgb2-aug | pruned-4gram-mix | - | - |
| end-to-end (deepspeech)  | -| - | - | - |
| end-to-end (char2wav)  | -| - | - | - |

### Rescoring

| Model  | Data | LM | mgb2-wer-dev | mgb3-wer-dev.Ali |
| ------------- | ------------- | ------------- | ------------- | ------------- |
| chain-tdnn-blstm  | mgb2-aug | 4-gram-mgb2 | - | - |
| chain-tdnn-blstm  | mgb2-aug | 4-gram-mix-best | - | - |

## Semi-Supervised

Supervised MGB2 (80k) + Unsupervised MGB2


| Model  | Data-sup | Data-unsup | LM | wer-dev | wer-test |
| ------------- | ------------- | ------------- | ------------- | ------------- | ------------- |
| chain-tdnn  | 80k | 80k | pruned-4gram | - | - |
| chain-tdnn  | 80k | 120k | pruned-4gram | - | - |
| chain-tdnn | 80k | 220k | pruned-4gram | - | - |


